import "pixi-spine";
// import * as PIXI from "pixi.js";

window.PIXI = PIXI;

var app = new PIXI.Application({
  width: 1000,
  height: 800,
  backgroundColor: 0x111111
});
document.body.appendChild(app.view);

app.loader
    .add("dragon_1", "assets/Dragon_Skin.json", {
      crossOrigin: true
    })
    .load(onAssetsLoaded);

function onAssetsLoaded(loader, res) {
  var spine = app.stage.addChild(new PIXI.spine.Spine(res.dragon_1.spineData));
  spine.position.set(350, 250);
  spine.skeleton.setSkinByName(`Rong_XH`);
  spine.state.setAnimation(0, `Bloom1`, false);
  spine.state.setAnimation(1, `Bloom2`, true);
  spine.state.setAnimation(2, `Bloom3`, true);
}
